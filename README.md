# Le test d’Ishihara #


## Cahier des charges ## 

Le but de ce projet est de réaliser un système de classification de signaux de couleur
à partir d'une image. Le système doit être capable de classifier les signaux de couleur.
Les signaux de couleur sont des signaux lumineux envoyé sur un écran. Ces signaux seront detecté par un petit robot, posé sur l'écran et qui à l'aide de caméra pourra détecter les signaux de couleur.

## Ressources ##
### Materiel ###
- Robot avec ses caméras
- Ecran correspondant à l'arène où le robot sera posé
- 4 ou 6 couleurs de signaux lumineux

### Logiciel ###
- Python 3.11

### Temps ###
- ± 8 heures

## Périmètre livrables ##
Le projet est composé de 3 parties :
- La première partie est la réalisation d'un système de classification de signaux de couleur à partir de 4 couleurs possibles.
- La deuxième partie est la réalisation d'un système de classification de signaux de couleur à partir de 6 couleurs possibles sur une durée variable non connue à l’avance.
- La troisième partie est la réalisation d'un système de classification de signaux de couleur à partir de 6 couleurs possibles, chacune de ses couleurs son associés a une fréquence en Hertz. Le robot emmettra un signal sonore correspondant à la fréquence de la couleur détectée.


## How to build ? ##
### Dependances ###
Le projet est réalisé en python 3.11. Il est nécessaire d'installer les librairies suivantes :
- numpy
- matplotlib
- paho-mqtt
- pillow
- seaborn
- sklearn


### IDE ###
Le projet est réalisé avec l'IDE VSCode.

### Bash ###
Le projet est réalisé avec le bash de git.

## How to run ? ##
Avant de lancer le projet veuillez deziper le dossier "data" à la racine du projet.
Ensuite suivez les étapes suivantes :
- Lancer le script "main.py" avec la commande suivante :
```bash
python3 main.py
```
- Suivez les instructions de la console

## Caractérisation ##
### Graphique matplotlib ###
![Alt text](confusion_matrix.png "Matrice de confusion")

Voici la matrice de confusion du model

On peut voir que cette matrice n'est pas comme les matrices classiques de classifier, cela témoigne que mon modèle n'est pas bon.


### Causalité du phénomène à classifier ###
Le phénomène à classifier est la détection de signaux de couleur. Ces signaux sont des signaux lumineux envoyé sur un écran. Ces signaux seront detecté par un petit robot, posé sur l'écran et qui à l'aide de caméra pourra détecter les signaux de couleur.

### Explication des métriques input ###
Afin caractériser les signaux de couleur, nous avons décidé de prendre en compte 1 paramètre :
    - La moyenne de la couleur des 10 dernieres lignes de pixels de l'image

### Classes à prédire output ###
Les classes à prédire sont les couleurs des signaux de couleur. Il y a 6 couleurs possibles :
    - Rouge
    - Jaune
    - Vert
    - Cyan
    - Bleu
    - Magenta

## Crédits ##
L'api MQTT a été réalisé par Juliens Arné. le projet SymphonX s'inscrit dans la continuité du projet pytactX développé par Julien Arné.
### License ###
Ce projet est sous licence MIT






