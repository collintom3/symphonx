import pickle
from statistics import mean
from matplotlib import pyplot as plt
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from ova import *
import os
import time
from MyRobot import *
from PIL import Image
import seaborn as sns

robot = MyRobot()

DATADIR = "data"
CATEGORIES = ['red','yellow','green','cyan', 'blue', 'magenta', 'black']
IMG_SIZE=100

#Prompt pour savoir si on veut faire de l'entrainement ou de la prédiction
def prompt():
	print("1. Entrainer le modèle")
	print("2. Prédire")
	print("3. Generate data")
	print("4. Voir la matrice de confusion")
	print("5. Quitter")
	choice = input("Choix: ")
	return choice


def record():
	choice = prompt()
	if choice == "1":
		while True:
				choice = input("Appuyer sur entrer pour démarrer: ")
				if choice == "":
					break
		start_time_global = time.time()
		while (time.time() - start_time_global) < 20:		
			for category in CATEGORIES:
				path = DATADIR + '/' + category
				robot.setActualPath(path)
				start_time_color = time.time()
				while (time.time() - start_time_color) < 2:
					robot.update()
					time.sleep(0.1)
	elif choice == "2":
		filename = 'finalized_model.sav'
		analyze_img(pickle.load(open(filename, 'rb')))
	elif choice == "3":
		create_data()
	elif choice == "4":
		create_confusion_matrix()
	elif choice == "5":
		exit()
	else:
		print("Choix invalide")
		prompt()

def analyze_img(model: SVC):
	previous_color = ""
	while True:
		robot.update()
		colors = []
		for y in range(230, 240, 1):
			for x in range(230, 240, 1):
				color = robot.getImagePixelRGB(x,y)
				colors.append(color)
		average_color = tuple(round(mean(channel)) for channel in zip(*colors))
		actual_color = prediction(model, average_color)
		if actual_color != previous_color:
			if actual_color[0] == "red":
				#setLesColor en rgb
				robot.setLedColor(255,0,0)
			elif actual_color[0] == "yellow":
				robot.setLedColor(255,255,0)
			elif actual_color[0] == "green":
				robot.setLedColor(0,255,0)
			elif actual_color[0] == "cyan":
				robot.setLedColor(0,255,255)
			elif actual_color[0] == "blue":
				robot.setLedColor(0,0,255)
			elif actual_color[0] == "magenta":
				robot.setLedColor(255,0,255)
			previous_color = actual_color[0]
		
def get_average_color(image):
	colors = []
	average_color = (0,0,0)
	for y in range(230, 240, 1):
		for x in range(230, 240, 1):
			color = image.getpixel((x,y))
			colors.append(color)
	average_color = tuple(round(mean(channel)) for channel in zip(*colors))
	return average_color
	

def create_data():
	data = []
	labels = []
	for category in CATEGORIES:
		path = DATADIR + '/' + category
		for img in os.listdir(path):
			image = Image.open(path + '/' + img)
			data.append(get_average_color(image))
			labels.append(category)
	df = pd.DataFrame(data)
	df['labels'] = labels

	# Sauvegarder les données dans un fichier csv à la racine du projet
	df.to_csv('test_data.csv')

def create_confusion_matrix():
	with open('finalized_model.sav', 'rb') as f:
		model = pickle.load(f)

	# Charger les données de test
	test_data = pd.read_csv('test_data.csv')
	# Séparer les données et les étiquettes
	#Take all the columns except the last one
	X_test = test_data.iloc[:, :-1].values[:, -3:]
	y_test = test_data['labels']

	# Prédire les étiquettes des données de test
	y_pred = model.predict(X_test)
	

	# Générer la matrice de confusion
	confusion_mtx = confusion_matrix(y_test, y_pred)
	# Créer un schéma de matrice de confusion avec Matplotlib
	sns.set(font_scale=1.4)
	sns.heatmap(confusion_mtx, annot=True, annot_kws={"size": 16}, cmap='Blues', fmt='g')
	
	plt.title('Matrice de confusion')
	plt.xlabel('Valeurs prédites')
	plt.ylabel('Valeurs réelles')
	#Afficher la matrice et lorsque l'utilisateur appuie sur entrer on ferme la fenêtre
	plt.show()
	input("Appuyer sur entrer pour fermer la fenêtre")
	plt.close()



def create_model(data, labels):
	print('Séparer les données en train et test')
	x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=42)
	print('Créer le modele svm')
	classifier = SVC(kernel='linear',random_state=0)
	classifier.fit(x_train, y_train)
	filename = 'finalized_model.sav'
	pickle.dump(classifier, open(filename, 'wb'))
	return x_train, x_test, y_train, y_test


def prediction(classifier: SVC, x_test):
	ypred = classifier.predict([x_test])
	print(ypred[0])
	return ypred

while True:
	record()
	os.system('cls') 
	
	# Si le robot est déconnecté
	if ( robot.isConnected() == False ):
		print("🔴",robot.getRobotId(),"déconnectée")

	# Si le robot est connecté
	else:
		# Affiche l'état et les capteurs du robot
		print("🟢",robot.getRobotId(),"connectée")
		print(f"🔋batterie {robot.getBatteryLevel()}% 💡avant {robot.getFrontLuminosityLevel()}% 💡arrière {robot.getBackLuminosityLevel()}%")

		# Affiche la taille de l'image reçue
		imageLargeur = robot.getImageWidth()
		imageHauteur = robot.getImageHeight()
		print("🖼️  Image reçue: " + str(imageLargeur) + "x" + str(imageHauteur) + "")		
