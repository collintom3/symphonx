import time

from ova import OvaClientMqtt

class MyRobot(OvaClientMqtt):
	def __init__(self):
		super().__init__(id="ova1097bdcc9931", arena="ishihara", username="", password="", server="192.168.10.103", 
		port=1883)
		self.actualPath = ""
		self.path = ""
		self.counter = 1
	
	def setActualPath(self, path):
		self.actualPath = path

	def _onImageReceived(self, img):
		img.save(self.actualPath + "/" + str(self.counter) + ".jpeg")
		self.counter += 1
		print("Image enregistrée")